# Description:
#   Javabutton
#   
# Commands:
#   hubot java - java fortune
# 
# Notes:
#   

kakugen = require './kakugen.json'

getRandomInt = (max) ->
  Math.floor Math.random() * Math.floor(max)

module.exports = (robot) ->
  robot.respond /java/, (msg) ->
    kakugen_index = getRandomInt kakugen.length
    msg.send kakugen[kakugen_index].t

